#import "shell32.dll"
   int ShellExecuteW(int hWnd, string Verb, string File, string Parameter, string Path, int ShowCommand);
#import

int OnStart()
{    
      //Shell("C:\\Python34\\python.exe", "'C:\\Program Files\\Alpari Limited MT4\\MQL4\\Projects\\python.py'");
      //Alert(TerminalPath());
      string json = StringConcatenate("\"{'c_s':", 1, ",",
                                      "'t_start':'", "06:00:00", "',",
                                      "'t_end':'", "23:00:00", "',",
                                      "'dir':'", TerminalPath(), "\\MQL4\\Files\\Ticks\\Alpari-Demo\\", Year(), "',",
                                      "'export':'", "C:\\", "',",
                                      "'s/l':", 0.5, ",",
                                      "'c_m':", 0.5, ",",  // % количества убыточных сделок
                                      "'lot':", 1, ",",
                                      "'t/p':", 0.05, "}\"");
      string scriptPath = "C:\\Python34\\python.exe \"C:\\Program Files\\Alpari Limited MT4\\MQL4\\Projects\\python.py\"";
      Shell(scriptPath, json);
      Alert(json);
      /*Alert(TimeCurrent());*/
      return(true);
} 

bool Shell(string file, string parameters=""){
    #define DEFDIRECTORY NULL
    #define OPERATION "open"       
    #define SW_HIDE             0   
    #define SW_SHOWNORMAL       1
    #define SW_NORMAL           1
    #define SW_SHOWMINIMIZED    2
    #define SW_SHOWMAXIMIZED    3
    #define SW_MAXIMIZE         3
    #define SW_SHOWNOACTIVATE   4
    #define SW_SHOW             5
    #define SW_MINIMIZE         6
    #define SW_SHOWMINNOACTIVE  7
    #define SW_SHOWNA           8
    #define SW_RESTORE          9
    #define SW_SHOWDEFAULT      10
    #define SW_FORCEMINIMIZE    11
    #define SW_MAX              11
    int r=ShellExecuteW(0, OPERATION, file, parameters, DEFDIRECTORY, SW_SHOW);
    if (r <= 32){   Alert("Shell failed: ", r); return(false);  }
    return(true);
}