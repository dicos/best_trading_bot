import csv
import glob
import json
import os.path
import math
import random
import sqlite3
import sys
from datetime import date, datetime, timedelta

from airspeed.api import Airspeed


today = date.today()


def get_old_balance(date_str):
    conn = sqlite3.connect('db.sqlite3')
    c = conn.cursor()
    query = "SELECT balance, ticket FROM balances WHERE date_saving < '%s' ORDER BY date_saving DESC"
    try:
        c.execute(query %date_str)
    except sqlite3.OperationalError:
        db_create = """ CREATE TABLE balances(
                        date_saving text,
                        balance real,
                        ticket int)"""
        c.execute(db_create)
        c.execute("CREATE UNIQUE INDEX balances_date_saving ON balances(date_saving);")
        conn.commit()
        return 10000, 54268054
    result = c.fetchone()
    conn.close()
    if not result:
        return 100000, 54268054
    return result


def save_balance(date_str, balance, ticket):
    conn = sqlite3.connect('db.sqlite3')
    c = conn.cursor()
    sql = "SELECT date_saving FROM balances WHERE date_saving='%s'"
    c.execute(sql % date_str)
    if c.fetchone():
        query = "UPDATE balances SET balance='%s', ticket='%d' WHERE date_saving='%s'"
    else:
        query = "INSERT INTO balances (balance, ticket, date_saving) VALUES ('%s', '%d', '%s')"
    c.execute(query % (balance, ticket, date_str))
    conn.commit()
    conn.close()


def get_name(f_name):
    return os.path.basename(f_name).split("_")[1][:-4]


def get_month_day(name):
    strday, remainder = os.path.basename(name).split("-")
    strmonth = remainder.split("_")[0]
    return int(strmonth), int(strday)


def get_filenames(input_data):
    pattern = os.path.abspath(os.path.join(input_data['dir'], "*.csv"))
    filenames_all = glob.glob(pattern)
    filenames_all.sort(reverse=True)
    month_day = get_month_day(filenames_all[0])
    return [f for f in filenames_all if month_day == get_month_day(f)]


def get_dates(input_data):
    start = datetime.strptime(input_data['t_start'], '%H:%M:%S')
    end = datetime.strptime(input_data['t_end'], '%H:%M:%S')
    return start, end


def bar_generator(f_name, time_start, time_end, count_selling):
    import csv # Баг вайна
    td = time_end - time_start
    period_seconds = math.floor(td.total_seconds() / count_selling)
    td_next = timedelta(seconds=period_seconds)
    csv_reader = csv.reader(open(f_name, 'r'), delimiter=";")
    bar = None
    for row in csv_reader:
        try:
            this_time = datetime.strptime(row[0], "%Y.%m.%d %H:%M:%S")
        except ValueError:
            continue
        if this_time.time() < time_start.time():
            continue
        try:
            price = float(row[1])
        except ValueError:
            continue
        if not bar:
            curr_time = time_start
            next_time = curr_time + td_next
            bar = {'min': price, 'max': price, 'min_time': this_time, 'max_time': this_time}
        if this_time.time() > next_time.time():
            if bar['min'] != bar['max']:
                yield bar
            while this_time.time() > next_time.time(): 
                curr_time = next_time
                next_time += td_next
            bar = {'min': price, 'max': price, 'min_time': this_time, 'max_time': this_time}
            continue
        if bar['min'] > price:
            bar.update({'min': price, 'min_time': this_time})
        elif bar['max'] < price:
            bar.update({'max': price, 'max_time': this_time})



def execute(str_json):
    import csv  # баг вайна
    input_data = json.loads(str_json) 
    time_start, time_end = get_dates(input_data)
    count_selling = input_data['c_s']
    bars = []
    today = date.today()
    f_report_name = 'report-%s-%s-%s' % (today.year, today.month, today.day,)
    csv_name = os.path.abspath(os.path.join(input_data['export'], "%s.csv" % f_report_name))
    d_file = csv.writer(open(csv_name, 'w'))
    start_row = ('Ticket', 'Open Time', 'Type', 'Size', 'Item', 'Price', 'S/L',
                 'T/P', 'Close Time', 'Price', 'Profit')
    d_file.writerow(start_row)
    stop_loss = float(input_data['s/l'])
    take_profit = float(input_data['t/p'])
    lot = input_data['lot']
    curr_date = date.today().strftime("%Y.%m.%d")
    for number, f_name in enumerate(get_filenames(input_data)):
        name = get_name(f_name)
        bar = None
        for bar in bar_generator(f_name, time_start, time_end, count_selling):
            # только прибыльные
            row = [0]
            if bar['min_time'].time() > bar['max_time'].time():
                row.extend((bar['max_time'], 'sell', lot, name, bar['max'], 
                            round(bar['max'] * (1 + stop_loss), 5),
                            bar['min'],
                            bar['min_time'], bar['min']))
            else:
                row.extend((bar['min_time'], 'buy', lot, name, bar['min'], 
                            round(bar['min'] * (1 - stop_loss), 5),
                            bar['max'],
                            bar['max_time'], bar['max']))
            row.append(round((bar['max'] - bar['min']) * lot, 5))
            for num, item in enumerate(row):
                if isinstance(item, datetime):
                    row[num] = item.strftime("%Y.%m.%d %H:%M:%S")
            bars.append(row)
        if bar:
            curr_date = bar['min_time'].strftime("%Y.%m.%d")
    random.shuffle(bars)
    count_bad = int(round(len(bars) * input_data['c_m']))
    for num, info in enumerate(bars[:count_bad]):
        bars[num][-1] = -info[-1] # Прибыль
        bars[num][2] = 'sell' if info[2] == 'buy' else 'buy'
        # стоп-лосс, тейк-профит
        bars[num][6], bars[num][7] = info[7], info[6]
    bars.sort(key=lambda x: x[1])

    old_balance, base_ticket = get_old_balance(curr_date)
    for num, info in enumerate(bars):
        base_ticket += random.randint(10, 4000)
        bars[num][0] = base_ticket

    d_file.writerows(bars)
    profit = round(sum((s[-1] for s in bars)), 5)
    new_balance = old_balance + profit
    save_balance(curr_date, new_balance, base_ticket)
    data = {'bars': bars,
            'sum': profit,
            'old_balance': old_balance,
            'new_balance': new_balance}
    template_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'template.html'))
    text = Airspeed()(data, template_path)[1]
    html_name = os.path.abspath(os.path.join(input_data['export'], "%s.html" % f_report_name))
    with open(html_name, 'w') as f:
        f.write(text)


if __name__ == "__main__":
    if sys.platform.startswith('linux'):
        tmp_data = """{'dir': '/home/dmitry/.wine/drive_c/Program Files/Alpari Limited MT4/MQL4/Files/Ticks/Alpari-Demo/2015',
                'export': '/home/dmitry/.wine/drive_c/Program Files/Alpari Limited MT4/MQL4/Files/',
                'c_s': 20,
                's/l': 0.05,
                't/p': 0.05,
                't_start': '07:00:00',
                'c_m': 0.15,
                'lot': 10,
                't_end': '23:00:00'}"""
    else:
        tmp_data = """{'dir': 'C:\\Program Files\\Alpari Limited MT4\\MQL4\\Files\\Ticks\\Alpari-Demo\\2015',
                'export': 'C:\\Program Files\\Alpari Limited MT4\\MQL4\\Files\\',
                'c_s': 20,
                's/l': 0.05,
                't/p': 0.05,
                't_start': '07:00:00',
                'c_m': 0.15,
                'lot': 10,
                't_end': '23:00:00'}"""
    data = sys.argv[1] if len(sys.argv) > 1 else tmp_data
    data = data.replace("'", "\"").replace("\\", "\\\\")
    execute(data)
