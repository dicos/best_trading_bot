#import "shell32.dll"
   int ShellExecuteW(int hWnd, string Verb, string File, string Parameter, string Path, int ShowCommand);
#import

#property copyright		"Copyright � 2006-2015, komposter; Dmitry Kosolapov 2015"
#property link				"http://www.komposter.me, http://dicos.ru"
#property version	 		"3.0"
#property strict

input string ExportReport = "C:\\";
input string	SymbolsList		= "EURUSD,GBPUSD,AUDUSD,USDCAD,USDJPY,USDCHF";
input string StrTimeStart = "06:00:00";
input string StrTimeEnd = "23:30:00";
input int IntCountSelling = 50;
input double StopLoss = 0.3;
input double TakeProfit = 0.11;
input double CountLossTrade = 0.15;
input uint Lot = 1;

string SymbolsArray[32];
double preBid[32];
int FileHandle[32], SymbolsCount = 0;

string	ServerName		= "";
datetime CurStartDateTime = TimeCurrent();
datetime CurEndDateTime = TimeCurrent();
bool IsRun = False;
int CurDay = Day();

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void InitDate()
{
	string StartTimeStr = StringConcatenate(Year(), ".", strMonth(), ".", strDay(), " ", StrTimeStart);
	string EndTimeStr = StringConcatenate(Year(), ".", strMonth(), ".", strDay(), " ", StrTimeEnd);
	CurStartDateTime = StrToTime(StartTimeStr);
	CurEndDateTime = StrToTime(EndTimeStr);
	CurDay = Day();
}


int OnInit()
{
	ServerName		= AccountServer();
	InitDate();


	// ��������� �� ������ SymbolsList ������ ��������
	if ( !PrepareSymbolsList() ) { return(INIT_FAILED); }

	// ��������� �����
	if ( !OpenFiles() ) { return(INIT_FAILED); }

	// ��������� ������
	if ( !EventSetMillisecondTimer( 100 ) )
	{
		Alert( "Can't set the timer!" );
		return(INIT_FAILED);
	}

	return(INIT_SUCCEEDED);
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
	// ��������� �����
	CloseFiles();
	Comment("");
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

void InitFiles()
{
	CloseFiles();
	if ( PrepareSymbolsList() && OpenFiles() )
	{
		InitDate();
		IsRun = True;
	}
}

void OnTimer()
{
	datetime CurTime = TimeCurrent();
	if (CurStartDateTime <= CurTime && CurEndDateTime > CurTime) {
	   if (ServerName != AccountServer() || !IsRun ) {
   	   InitFiles();
      }
      // ���������� ����������� ����
      WriteTick();
   } else if (IsRun) {
      IsRun = False;
      CloseFiles();
      //Alert("Da");
      string json = StringConcatenate("\"{'c_s':", IntCountSelling, ",",
                                      "'t_start':'", StrTimeStart, "',",
                                      "'t_end':'", StrTimeEnd, "',",
                                      "'dir':'", TerminalPath(), "\\MQL4\\Files\\Ticks\\Alpari-Demo\\", Year(), "',",
                                      "'export':'", ExportReport, "',",
                                      "'s/l':", StopLoss, ",",
                                      "'c_m':", CountLossTrade, ",",
                                      "'lot':", Lot, ",",
                                      "'t/p':", TakeProfit, "}\"");
      string scriptPath = "C:\\Python34\\python.exe \"C:\\Program Files\\Alpari Limited MT4\\MQL4\\Projects\\python.py\"";
     Shell(scriptPath, json);
   }
   if (CurDay != Day()) {
      InitDate();
   }
}

//+------------------------------------------------------------------+
//| ��������� �� ������ SymbolsList ������ ��������
//+------------------------------------------------------------------+
bool PrepareSymbolsList()
{
	int		curchar = 0;
	int		len = StringLen( SymbolsList ), curSymbol;
	string	cur_symbol = ""; SymbolsCount = 0;

	//---- ������������� ������������� ������ ������� ��������
	ArrayResize( SymbolsArray, 32 );

	//---- ���������� ��� ������� � ������ ��������
	for ( int pos = 0; pos <= len; pos ++ )
	{
		curchar = StringGetChar( SymbolsList, pos );
		//---- ���� ������� ������ - �� ������� � �� ��������� ������ � ������,
		if ( curchar != ',' && pos != len )
		{
			//---- ��� - ���� �� �������� ����������� �������
			cur_symbol = cur_symbol + CharToStr( uchar(curchar) );
			continue;
		}
		//---- ���� ������� ������ �������, ��� ��� - ��������� ������ � ������,
		else
		{ 
			//---- ������, � ���������� cur_symbol - ������ ��� �������. ��������� ���:
			MarketInfo( cur_symbol, MODE_BID );
			if ( cur_symbol == "" || GetLastError() == 4106 )
			{
				Alert( "����������� ������ \"", cur_symbol, "\"!!!" );
				return(false);
			}

			//---- ���� ������ ������� ����������, ���������, ��� �� ������ ������ � ����� ������:
			bool Uniq = true;
			for ( curSymbol = 0; curSymbol < SymbolsCount; curSymbol ++ )
			{
				if ( cur_symbol == SymbolsArray[curSymbol] )
				{
					Uniq = false;
					break;
				}
			}

			//---- ���� ������ ������ � ����� ������ ���, ���������� ���, � ������� ����� ����������:
			if ( Uniq )
			{
				SymbolsArray[SymbolsCount] = cur_symbol;
				SymbolsCount ++;
				if ( SymbolsCount > 31 )
				{
					Alert( "������� ����� ��������! ������� ����� �������� 32 �����!" );
					return(false);
				}
			}

			//---- �������� �������� ����������
			cur_symbol = "";
		}
	}

	//---- ���� �� ���� ������ �� ��� ������, �������
	if ( SymbolsCount <= 0 )
	{
		Alert( "�� ���������� �� ������ �������!!!" );
		return(false);
	}
	
	//---- ������������� ������ ���� �������� ��� ���-�� ��������:
	ArrayResize		( SymbolsArray	, SymbolsCount );
	ArrayResize		( preBid			, SymbolsCount );
	ArrayInitialize( preBid			, -1 				);
	ArrayResize		( FileHandle	, SymbolsCount );
	ArrayInitialize( FileHandle	, -1 				);

	//---- ������� ����������:
	string uniq_symbols_list = SymbolsArray[0];
	for ( curSymbol = 1; curSymbol < SymbolsCount; curSymbol ++ )
	{
		if ( curSymbol == SymbolsCount - 1 )
		{ uniq_symbols_list = uniq_symbols_list + " � " + SymbolsArray[curSymbol]; }
		else
		{ uniq_symbols_list = uniq_symbols_list + ", " + SymbolsArray[curSymbol]; }
	}
	Comment( AccountServer(), ": �������������� ", SymbolsCount, " ������(-�,-��):\n", uniq_symbols_list );

	return(true);
}

//+------------------------------------------------------------------+
//| ��������� �����, � ������� ����� ���������� ����
//+------------------------------------------------------------------+
bool OpenFiles()
{
	int _GetLastError;
	for ( int curSymbol = 0; curSymbol < SymbolsCount; curSymbol ++ )
	{
		string FileName = StringConcatenate( "Ticks\\", AccountServer(), "\\", Year(), "\\", strMonth(), "-", strDay(), "_", SymbolsArray[curSymbol], ".csv" );
		FileHandle[curSymbol] = FileOpen( FileName, FILE_READ | FILE_WRITE | FILE_CSV | FILE_ANSI | FILE_SHARE_READ );

		if ( FileHandle[curSymbol] < 0 )
		{
			_GetLastError = GetLastError();
			Alert( "FileOpen( " + FileName + ", FILE_READ | FILE_WRITE | FILE_CSV | FILE_ANSI | FILE_SHARE_READ ) - Error #", _GetLastError );
			return(false);
		}

		if ( !FileSeek( FileHandle[curSymbol], 0, SEEK_END ) )
		{
			_GetLastError = GetLastError();
			Alert( "FileSeek( ", FileHandle[curSymbol], ", 0, SEEK_END ) - Error #", _GetLastError );
			return(false);
		}

		preBid[curSymbol] = MarketInfo( SymbolsArray[curSymbol], MODE_BID );
	}
	return(true);
}

//+------------------------------------------------------------------+
//| ���������� ����������� ����
//+------------------------------------------------------------------+
void WriteTick()
{
	int _GetLastError; double curBid, curPoint; int curDigits;
	for ( int curSymbol = 0; curSymbol < SymbolsCount; curSymbol ++ )
	{
		if ( FileHandle[curSymbol] < 0 ) { continue; }

		curBid = MarketInfo( SymbolsArray[curSymbol], MODE_BID );
		curDigits = (int)MarketInfo( SymbolsArray[curSymbol], MODE_DIGITS );
		curPoint = MarketInfo( SymbolsArray[curSymbol], MODE_POINT );

		if ( MathAbs( curBid - preBid[curSymbol] ) < curPoint/2.0 ) { continue; }

		if ( FileWrite( FileHandle[curSymbol], TimeToStr( TimeCurrent(), TIME_DATE | TIME_SECONDS ), DoubleToStr( curBid, curDigits ) ) <= 0 )
		{
			_GetLastError = GetLastError();
			Alert( "FileWrite() Error #", _GetLastError );
		}

		preBid[curSymbol] = curBid;

		FileFlush( FileHandle[curSymbol] );
	}
}

//+------------------------------------------------------------------+
//| ��������� ��� �����
//+------------------------------------------------------------------+
void CloseFiles()
{
	for ( int curSymbol = 0; curSymbol < SymbolsCount; curSymbol ++ )
	{
		if ( FileHandle[curSymbol] > 0 )
		{
			FileClose( FileHandle[curSymbol] );
			FileHandle[curSymbol] = -1;
		}
	}
}

string strMonth()
{
	if ( Month() < 10 ) return( StringConcatenate( "0", Month() ) );
	return(string(Month()));
}

string strDay()
{
   if (Day() < 10) return StringConcatenate("0", Day());
   return string(Day());
}

bool Shell(string file, string parameters=""){
    #define DEFDIRECTORY NULL
    #define OPERATION "open"       
    #define SW_HIDE             0   
    #define SW_SHOWNORMAL       1
    #define SW_NORMAL           1
    #define SW_SHOWMINIMIZED    2
    #define SW_SHOWMAXIMIZED    3
    #define SW_MAXIMIZE         3
    #define SW_SHOWNOACTIVATE   4
    #define SW_SHOW             5
    #define SW_MINIMIZE         6
    #define SW_SHOWMINNOACTIVE  7
    #define SW_SHOWNA           8
    #define SW_RESTORE          9
    #define SW_SHOWDEFAULT      10
    #define SW_FORCEMINIMIZE    11
    #define SW_MAX              11
    int r=ShellExecuteW(0, OPERATION, file, parameters, DEFDIRECTORY, SW_SHOW);
    if (r <= 32){   Alert("Shell failed: ", r); return(false);  }
    return(true);
}
